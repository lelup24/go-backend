package main

import (
	router "backend/main/router"
	"log"
	"net/http"
)

func main() {

	router := router.NewRouter()

	go log.Fatal(http.ListenAndServe(":8080", router))

}



