package user

import (
	"backend/main/db"
	"log"
)

func GetSingleUser(u *User, params map[string]string) {
	db.Init()
	if params["id"] == "" &&  params["name"] == "" {
		return
	}

	rows, err := db.BuildQuery("SELECT * FROM user where ", params)

	if err != nil {
		panic(err)
	}

	count, err := rows.Columns()

	for rows.Next() && len(count) > 0 {
		err = rows.Scan(&u.Id, &u.Name)
	}

	defer db.DB.Close()


	if err != nil {
		panic(err)
	}
}

func Save(u *User) {

	db.Init()
	i, err := db.DB.Prepare("INSERT INTO user (name) VALUES (?)")

	if err != nil {
		log.Fatal(err)
	}

	i.Exec(u.Name)

	defer db.DB.Close()

}

func Update(u *User) {

	db.Init()
	i, err := db.DB.Prepare("UPDATE user SET name = ? WHERE id = ?")

	if err != nil {
		log.Fatal(err)
	}

	i.Exec(u.Name, u.Id)

	defer db.DB.Close()

}

func Delete(u *User) {

	db.Init()
	i, err := db.DB.Prepare("DELETE FROM user WHERE id = ?")

	if err != nil {
		log.Fatal(err)
	}

	i.Exec(u.Id)

	defer db.DB.Close()

}


func ServeList(params map[string]string) []User {

	db.Init()

	stmt := "SELECT * FROM user where "

	rows, err := db.BuildQuery(stmt, params)


	if err != nil {
		panic(err)
	}

	count, err := rows.Columns()

	t := make([]User, 0)
	var u User

	for rows.Next() && len(count) > 0 {
		err = rows.Scan(&u.Id, &u.Name)
		t = append(t, User{u.Id, u.Name})
	}

	defer db.DB.Close()
	if err != nil {
		panic(err)
	}

	return t

}