package user

import (
	"encoding/json"
	"log"
	"net/http"
)

const (
	ID = "id"
	NAME = "name"
	LIMIT = "limit"
	OFFSET = "offset"
)

func UserPage(w http.ResponseWriter, r *http.Request) {

	var u User
	values := map[string]string{ID: "", NAME: ""}

	for s := range values {
		if r.FormValue(s) != "" {
			values[s] = r.FormValue(s)
		}
	}

	GetSingleUser(&u, values)

	w.Header().Set("Content-Type", "application/json; chartset=utf-8")
	w.WriteHeader(http.StatusOK)


	err := json.NewEncoder(w).Encode(u)
	if err != nil {
		log.Fatal(err)
	}
}

func AllUser(w http.ResponseWriter, r *http.Request) {

	var users []User

	values := map[string]string{ID: "", NAME: "", LIMIT: "", OFFSET: ""}

	for s := range values {
		if r.FormValue(s) != "" {
			values[s] = r.FormValue(s)
		}
	}

	users = ServeList(values)

	w.Header().Set("Content-Type", "application/json; chartset=utf-8")
	w.WriteHeader(http.StatusOK)


	err := json.NewEncoder(w).Encode(users)
	if err != nil {
		log.Fatal(err)
	}

}

func CreateUser(w http.ResponseWriter, r * http.Request) {

	var u *User = &User{}

	err := json.NewDecoder(r.Body).Decode(u)

	if err != nil {
		log.Fatal(err)
	}

	Save(u)

	w.Header().Set("Content-Type", "application/json; chartset=utf-8")
	w.WriteHeader(http.StatusOK)

}

func UpdateUser(w http.ResponseWriter, r * http.Request) {

	var u *User = &User{}

	err := json.NewDecoder(r.Body).Decode(u)

	if err != nil {
		log.Fatal(err)
	}

	Update(u)

	w.Header().Set("Content-Type", "application/json; chartset=utf-8")
	w.WriteHeader(http.StatusOK)

}

func DeleteUser(w http.ResponseWriter, r * http.Request) {

	var u *User = &User{}

	u.Id = r.FormValue("id")

	Delete(u)

	w.Header().Set("Content-Type", "application/json; chartset=utf-8")
	w.WriteHeader(http.StatusOK)

}