package db

import (
	"database/sql"
	_"github.com/go-sql-driver/mysql"
)

func ConnectToMysql() *sql.DB {

	DB, err := sql.Open("mysql", "admin:admin@tcp(localhost:3306)/go")

	if err != nil {
		panic(err.Error())
	}

	return DB
}
