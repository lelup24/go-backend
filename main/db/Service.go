package db

import (
	"database/sql"
	"strings"
)


func BuildQuery(stmt string, params map[string]string) (*sql.Rows, error) {

	var where []string
	var args []interface{};
	var limit string = "10"
	var offset string = "0"

	i := 0
	for key, _ := range params {
		if params[key] != "" && key != "limit" && key != "offset" {
			where = append(where, key + " = ?")
			args = append(args, params[key])
			i++
		}
		if key == "limit" && params[key] != "" {
			limit = params["limit"]
		}

		if key == "offset" && params[key] != "" {
			offset = params["offset"]
		}

	}

	if len(args) == 0 {
		where = []string{""}
		stmt = strings.Replace(stmt, "where", "", 1)
	}

	stmt = stmt + strings.Join(where, " and ") + " LIMIT " + limit + " OFFSET " + offset

	println(stmt)

	return DB.Query(stmt, args...)
}
