package router

import (
	"backend/main/user"
	"encoding/json"
	"net/http"
)

type Route struct {
	Name        string
	Method      string
	Pattern     string
	HandlerFunc http.HandlerFunc
}

type _Routes []Route

var routes = _Routes{
	Route{
		"Index",
		"GET",
		"/", Index,
	},
	Route{
		"All User",
		"GET",
		"/api/users", user.AllUser,
	},
	Route{
		"User",
		"GET",
		"/api/user", user.UserPage,
	},
	Route{
		"User",
		"POST",
		"/api/user", user.CreateUser,
	},
	Route{
		"User",
		"PUT",
		"/api/user", user.UpdateUser,
	},
	Route{
		"User",
		"DELETE",
		"/api/user", user.DeleteUser,
	},

}

func Index(w http.ResponseWriter, r *http.Request) {

	json.NewEncoder(w).Encode("Welcome")

}